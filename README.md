# sudokuer



## Purpose

this is a pet project where I test out sudoku solving algorithms.

## How it works
The idea is to 
#### 1) type in a sudoku puzzle by providing the numbers one by one
    #7 1 5  
this will insert 7 in the 1st row - 5th column cell.  
Not a 7? Type:  

    #- 1 5  
this will erase the contents of the 1st row - 5th column cell.
#### 2) ask the current solving algorithm
    can find next?
this will run the algorithm and reply where or not it can find at least a number that can be added in the board.  
You can also type

    next number?  
this will print out the first number the algorithm found that can be added in the board.  

## Current Algorithm under test

On each **turn** - aka valid number insertion - the algorithm makes sure that:  
a) all cells of the same **row**    know that they cannot have the inserted number  
c) all cells of the same **column** know that they cannot have the inserted number  
a) all cells of the same **box**    know that they cannot have the inserted number  

effectively on every turn, some cells update their list of available numbers.   

On each **check** - aka "can find next?" or "next number?" command - the algorithm checks:  
a) if there is a **row**    that only one cell can host one of the numbers 1 to 9   
b) if there is a **column** that only one cell can host one of the numbers 1 to 9   
c) if there is a **row**    that only one cell can host one of the numbers 1 to 9   
