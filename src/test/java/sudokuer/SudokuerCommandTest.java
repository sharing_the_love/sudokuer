package sudokuer;

import io.micronaut.context.ApplicationContext;
import io.micronaut.context.env.Environment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sudokuer.Screen.WAIT_FOR_COMMAND_SIGN;
import static sudokuer.Screen.WELCOME_MESSAGE;

public class SudokuerCommandTest {
    private final static String EXIT_COMMAND = "exit\n";
    private ByteArrayOutputStream outputStream;

    @BeforeEach
    void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    void whenRun_thenShowWelcomeMessage_thenShowBoard_thenShowWaitForCommandSign() {
        System.setIn(new ByteArrayInputStream(EXIT_COMMAND.getBytes()));

        try (ApplicationContext ctx = ApplicationContext.run(Environment.CLI, Environment.TEST)) {
            (new SudokuerCommand()).run();

            Scanner output = new Scanner(outputStream.toString());
            output.nextLine(); // first line contains info from the framework I don't want to test

            assertShowsWelcomeMessage(output);
            assertShowsEmptyBoard(output);
            assertShowsWaitForCommandSign(output);
        }
    }

    @Test
    void given715_whenRun_TheShow7In1stRow5thColumnCell() {
        System.setIn(new ByteArrayInputStream(("7 1 5\n" + EXIT_COMMAND).getBytes()));

        try (ApplicationContext ctx = ApplicationContext.run(Environment.CLI, Environment.TEST)) {
            (new SudokuerCommand()).run();

            Scanner output = new Scanner(outputStream.toString());
            output.nextLine(); // first line contains info from the framework I don't want to test

            assertShowsWelcomeMessage(output);
            assertShowsEmptyBoard(output);
            assertShowsBoardWith7At1stRow5thColumn(output);
            assertShowsWaitForCommandSign(output);
        }
    }

    private void assertShowsWelcomeMessage(Scanner output) {
        Arrays.stream(WELCOME_MESSAGE.split("\n")).forEach(line -> assertEquals(line, output.nextLine()));
    }

    private void assertShowsEmptyBoard(Scanner output) {
        assertEquals("┌───┬───┬───┰───┬───┬───┰───┬───┬───┐", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("└───┴───┴───┸───┴───┴───┸───┴───┴───┘", output.nextLine());
    }
    private void assertShowsBoardWith7At1stRow5thColumn(Scanner output) {
        assertEquals("#┌───┬───┬───┰───┬───┬───┰───┬───┬───┐", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃ 7 │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("├───┼───┼───╋───┼───┼───╋───┼───┼───┤", output.nextLine());
        assertEquals("│   │   │   ┃   │   │   ┃   │   │   │", output.nextLine());
        assertEquals("└───┴───┴───┸───┴───┴───┸───┴───┴───┘", output.nextLine());
    }

    private void assertShowsWaitForCommandSign(Scanner output) {
        assertEquals(WAIT_FOR_COMMAND_SIGN, output.nextLine());
    }
}
