package sudokuer;

import io.micronaut.configuration.picocli.PicocliRunner;

import picocli.CommandLine.Command;

import java.util.Scanner;

@Command(name = "sudokuer", description = "...",
        mixinStandardHelpOptions = true)
public class SudokuerCommand implements Runnable {

    public static void main(String[] args) {
        PicocliRunner.run(SudokuerCommand.class, args);
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        GameState gameState = new GameState(false);

        Screen.displayWelcomeMessage();
        Screen.displayBoard(gameState);

        while (true) {
            String command = scanner.nextLine();
            gameState = CommandRunner.run(command, gameState);

            if (gameState.isFinished()) return;

            Screen.displayBoard(gameState);
        }
    }
}
