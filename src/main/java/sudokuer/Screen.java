package sudokuer;

public class Screen {
    static final String WELCOME_MESSAGE = """
            Welcome to Sudokuer!
            The sudoku board is now empty.
            You can interact with it by typing commands next to the '#'""";
    static final String WAIT_FOR_COMMAND_SIGN = "#";

    static void displayWelcomeMessage() {
        System.out.println(WELCOME_MESSAGE);
    }

    static void displayBoard(GameState state) {
        System.out.println("┌───┬───┬───┰───┬───┬───┰───┬───┬───┐");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("├───┼───┼───╋───┼───┼───╋───┼───┼───┤");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("├───┼───┼───╋───┼───┼───╋───┼───┼───┤");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("├───┼───┼───╋───┼───┼───╋───┼───┼───┤");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("├───┼───┼───╋───┼───┼───╋───┼───┼───┤");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("├───┼───┼───╋───┼───┼───╋───┼───┼───┤");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("├───┼───┼───╋───┼───┼───╋───┼───┼───┤");
        System.out.println("│   │   │   ┃   │   │   ┃   │   │   │");
        System.out.println("└───┴───┴───┸───┴───┴───┸───┴───┴───┘");
        System.out.print(WAIT_FOR_COMMAND_SIGN);
    }
}
