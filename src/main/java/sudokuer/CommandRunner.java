package sudokuer;

public class CommandRunner {
    public static GameState run(String command, GameState gameState) {
        return switch (command.toLowerCase()) {
            case "exit", "e" -> new GameState(true);
            default -> new GameState(false);
        };
    }
}
