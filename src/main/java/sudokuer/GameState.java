package sudokuer;

public record GameState(Boolean isFinished) {
}
